<?php

class UserSessionModel extends CI_Model { 

    public function __construct() {
		parent::__construct();
        $this->load->database();
    }
    public function createSession($userdata){
        $this->load->model('Usermodel');
        $this->destroySession($userdata);
        $session = array(
            "token" => $this->__generateToken($userdata),
            "user_id" => $this->Usermodel->getUserid($userdata),
            "email" => $userdata['email'],
            "session_date" => date('Y-m-d H:i:s', time())
        );
        $this->session->set_userdata($session);
        $this->db->insert('users_sessions',$session);

        $userdetails = array(
            "ip" => $this->input->ip_address(),
            "user_agent" => $this->input->user_agent(),
            "date_last_login" => date('Y-m-d H:i:s', time())
        );
        $this->db->where('user_id', $this->session->userdata("user_id"));
        $this->db->update('users',$userdetails);
    }
    public function refreshSession(){
        $usersession = $this->session->all_userdata();
        if($this->checkSession()){

            $sessionDateStr = strtotime($this->session->userdata("session_date"));
            $currentDateStr = strtotime(date('Y-m-d H:i:s', time()));
            if( $currentDateStr - $sessionDateStr > 86400){ //dzień
                $this->createSession($userdata);
            }
        }else{
            $this->destroySession();
            redirect(site_url('user/login'));
        }
        return true;
    }
    public function destroySession($userdata = NULL){
        $usersession = $this->session->all_userdata();
        if(isset($usersession['user_id'])){
            $query = $this->db->query("DELETE FROM `users_sessions` WHERE `user_id` = '".$usersession['user_id']."'" );
        }else if(isset($usersession['email'])){
            $query = $this->db->query("DELETE FROM `users_sessions` WHERE `email` = '".$usersession['email']."'" );
        }else if(isset($userdata['user_id'])){
            $query = $this->db->query("DELETE FROM `users_sessions` WHERE `user_id` = '".$userdata['user_id']."'" );
        }else if(isset($userdata['email'])){
            $query = $this->db->query("DELETE FROM `users_sessions` WHERE `email` = '".$userdata['email']."'" );
        }
        //$this->session->sess_destroy();
    }
    public function checkSession(){
        $userdata = $this->session->all_userdata();
        if(isset($userdata['user_id']) && isset($userdata['email']) && isset($userdata['token']) ){
            $query = $this->db->query("SELECT `token` FROM `users_sessions` WHERE `user_id` = '".$userdata['user_id']."'");
            $result = $query->result();
            if($result){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
        
        
    }

    private function __generateToken($userdata){
		return md5(date('Y-m-d H:i:s', time()).$userdata['email'].$this->config->item('encryption_key'));
	}
}