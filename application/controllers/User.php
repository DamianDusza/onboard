<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Usermodel');
        if($this->Usersessionmodel->checkSession()){
            redirect(site_url('app/index'));
        }
    }

    public function index()
    {
        redirect(site_url('user/login'));
    }
    public function login(){

        $data = array(
            'google_redirect_uri' => $this->Usermodel->getGoogleAuthUrl()
        );
        $this->load->helper('form');
        $this->load->view('login', $data);
        
    }
    public function google(){
        if($this->input->get('code')) { 
            $token = $this->Usermodel->getGoogleAccessToken($this->input->get('code'));
            if($token){
                $this->create($this->Usermodel->getUserInfo($token));
            }else{
                var_dump('google authorization error');
            }
        }else{
            var_dump('google authorization error');
        }
    }
    public function authorization(){
        $userdata = $this->session->all_userdata();
        if($this->Usermodel->isUserActive($userdata)){
            if($this->Usersessionmodel->checkSession()){

                $this->Usersessionmodel->refreshSession();
                var_dump('redirect');
                var_dump($this->session->all_userdata());
                redirect(site_url('app/index'));
            }else{
                $this->Usersessionmodel->createSession($userdata);
            }
        }else{
            var_dump('activation menu');
            die();
        }
        if($this->Usermodel->isUserBanned($userdata)){
            var_dump('banned info');
            die();
        }
        if($this->Usermodel->isPasswordSet($userdata)){
            var_dump('password menu');
            die();
        }
    }
    public function create($userdata){
        if($this->Usermodel->isUserExist($userdata)){
            $this->Usersessionmodel->createSession($userdata); ;
            $this->authorization();                   
        }else{
            $this->Usermodel->createUser($userdata);
            $this->authorization($userdata);
        }
    }
    
}
