<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->Usersessionmodel->refreshSession();
	}
	public function index()
    {
    	redirect('app/dashboard');
    }
  	public function dashboard()
  	{
		echo "dashboard";
    }
}