<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Portal walutowy</title>
		<?= autoload_header_css(); ?>
		<?= autoload_header_js(); ?>
		<?php load_css(array('login.css'));?>

		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>	
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<section class="bg">
			<section class="login">
				<header>
					<img src="<?= base_url().IMG ?>logo-white.png" width="300" height="132"/>
				</header>
				<?php echo form_open('user/login'); ?>
					
					<?php if($this->session->flashdata('error')){ ?>
					<h3>
						<?php echo $this->session->flashdata('error'); ?>
					</h3>
					<?php } ?>
					<?php if(validation_errors()){ ?>
					<h3>
						<?php echo validation_errors(); ?>
					</h3>
					<?php } ?>
					<input type="email" name="email" id="email" placeholder="Adres e-mail...">
					<input type="password" name="password" name="password" placeholder="Hasło...">
					<input type="submit" value="Zaloguj"/>
					<a href="<?php echo $google_redirect_uri; ?>" target="_self">
						<span>Zaloguj się z Facebook</span>
						<img src="<?= base_url().IMG ?>icons/fb-white.png" width="50"/>
					</a>
				</form>
			</section>
		</section>
		
		<?= autoload_footer_js(); ?>
	</body>
</html>