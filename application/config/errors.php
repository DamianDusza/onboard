<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['errors']['authorization']['email_exist'] = "Podany adres e-mail już istnieje. Zaloguj się lub przypomnij hasło.";
