<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['header_css'] = array('reset.css','core.css', 'http://fonts.googleapis.com/css?family=Teko:300,700&subset=latin,latin-ext');
$config['header_js']  = array('jquery/jquery-1.11.2.min.js');
$config['footer_js'] = array('helpers.js', 'core.js',);
