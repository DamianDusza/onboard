<?php

function autoload_header_css()
{
    $ci = get_instance();
    $ci->load->config('autoload-front');
    load_css($ci->config->item('header_css'));
}
function autoload_header_js()
{
    $ci = get_instance();
    $ci->load->config('autoload-front');
    load_js($ci->config->item('header_js'));
}
function autoload_footer_js()
{
    $ci = get_instance();
    $ci->load->config('autoload-front');
    load_js($ci->config->item('footer_js'));
}

function load_css($files)
{
    foreach ($files as $file) {
    	if (strpos($file,'//') !== false) {
		   echo "<link href='".$file."' rel='stylesheet' type='text/css'>";
		}else{
			echo "<link href='".base_url().CSS.$file."' rel='stylesheet' type='text/css'>";
		}
    }
}
function load_js($files)
{
    foreach ($files as $file) {
    	if (strpos($file,'//') !== false) {
		    echo "<script src='".$file."''></script>";
		}else{
    		echo "<script src='".base_url().JS.$file."''></script>";
    	}
    }
}

?>