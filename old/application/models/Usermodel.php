<?php

class UserModel extends CI_Model { 

    public function __construct() {
		parent::__construct();
		$this->load->helper('user');
		$this->load->database();
    }

    public function index(){
    	return 1;
    }
	public function getGoogleAuthUrl(){
		$params = array(
		    "response_type" => "code",
		    "client_id" => $this->config->item('oauth')['client_id'],
		    "redirect_uri" => $this->config->item('oauth')['redirect_uri'],
		    "scope" => "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.email"
		);
    	return $this->config->item('oauth')['authUrl'] . '?' . http_build_query($params);
    }
    public function getGoogleAccessToken($code){

    	$clienttoken_post = array(
		   "code" => $code,
		   "client_id" => $this->config->item('oauth')['client_id'],
		   "client_secret" => $this->config->item('oauth')['client_secret'],
		   "redirect_uri" => $this->config->item('oauth')['redirect_uri'],
		   "grant_type" => "authorization_code",
	    );	     
	    $curl = curl_init($this->config->item('oauth')['tokenUrl']);
	 
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, $clienttoken_post);
	    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	 
	    $json_response = curl_exec($curl);
	    curl_close($curl);

	    $authObj = json_decode($json_response);
	    $accessToken = $authObj->access_token;
	    return $accessToken;
    }
    public function getUserInfo($token){
		$url = 'https://www.googleapis.com/oauth2/v1/userinfo?'.http_build_query(array(
			'access_token' => $token,
		));
		$userdata = get_object_vars(json_decode(file_get_contents($url)));
		return parseUserData($userdata, "google");
	}
    public function createUser($userdata){
    	$userdata += array(
    		"created_on" => date('Y-m-d h:i:s', time()),
    		"status" => "to confirm"
    	);
    	$this->db->insert('!users',$userdata);
    }
    public function createSession($userdata){
    	var_dump($this->__generateToken($userdata));
    	//$this->db->insert('!users',$userdata);
    }
    public function refreshSession($userdata){
    	
    }
    public function isUserExist($userdata){
    	
    	if(isset($userdata['email'])){
    		$query = $this->db->query("SELECT id FROM `!users` WHERE `email` = '".$userdata['email']."'");
    	}else if(isset($userdata['username'])){
    		$query = $this->db->query("SELECT id FROM `!users` WHERE `username` = '".$userdata['username']."'");
    	}else{
    		var_dump("showing authorization error");
    	}
    	if($query->result()){
    		return true;
    	}else{
    		return false;
    	}
    }
    public function isUserBanned($userdata){
    	if($this->getStatus($userdata)  == "banned"){
    		return true;
    	}else{
    		return false;
    	}
    }
    public function isUserActive($userdata){
    	if($this->getStatus($userdata)  == "active"){
    		return true;
    	}else{
    		return false;
    	}
    }
    public function isPasswordSet($userdata){
    	if(isset($userdata['email'])){
    		$query = $this->db->query("SELECT password FROM `!users` WHERE `email` = '".$userdata['email']."'");
    	}else if(isset($userdata['username'])){
    		$query = $this->db->query("SELECT password FROM `!users` WHERE `username` = '".$userdata['username']."'");
    	}else{
    		var_dump("showing query error");
    		die();
    	}
    	$result = $query->result();
    	if($result[0]->password == ''){
    		return false;
    	}else{
    		return true;
    	}
    }
    public function getStatus($userdata){
    	if(isset($userdata['email'])){
    		$query = $this->db->query("SELECT status FROM `!users` WHERE `email` = '".$userdata['email']."'");
    	}else if(isset($userdata['username'])){
    		$query = $this->db->query("SELECT status FROM `!users` WHERE `username` = '".$userdata['username']."'");
    	}else{
    		var_dump("showing query error");
    		die();
    	}
    	return $query->result()[0];
    }
    private function __generateToken($userdata){
		return md5(date('Y-m-d h:i:s', time()).$userdata['email'].$this->config->item('encryption_key'));
	}
}