<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->config->load('oauth');
        $this->load->model('Usermodel');
    }

    public function index()
    {
        header("Location: ".site_url('cms/login'));
    }
    public function login(){
        $data = array(
            'google_redirect_uri' => $this->Usermodel->getGoogleAuthUrl();
        );
        $this->load->view('login', $data);
    }
    public function google(){

        if($this->input->get('code')) { 
            $token = $this->Usermodel->getGoogleAccessToken($this->input->get('code'));
            if($token){
                $this->__create($this->Usermodel->getUserInfo($token));
            }else{
                var_dump('showing google authorization error');
            }
        }else{
            var_dump('showing google authorization error');
        }
    }
    private function __create($userdata){
        var_dump('isUserExist');
        var_dump($this->Usermodel->isUserExist($userdata));
        if($this->Usermodel->isUserExist($userdata)){
            $this->__authenticate($userdata);                   
        }else{
            $this->Usermodel->createUser($userdata);
            $this->__authenticate($userdata);
        }
    }
    private function __authenticate($userdata){

        if($this->Usermodel->isUserActive($userdata)){
            $this->Usermodel->createSession($userdata);
        }else{
            var_dump('showing activation menu');
            die();
        }
        if($this->Usermodel->isUserBanned($userdata)){
            var_dump('showing banned info');
            die();
        }
        if($this->Usermodel->isPasswordSet($userdata)){
            var_dump('showing password menu');
            die();
        }
    }
}
